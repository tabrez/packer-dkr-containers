# Setup basic development environment in a container

Packer builds a docker image with support for git, zsh, vim and pushes it to
gitlab's container registry.

## Pre-requisites to run the container

+ Docker Engine
+ Docker Compose (Optional)

## Run the container

```sh
# option 1
docker run --user ubuntu -w /home/ubuntu -it registry.gitlab.com/tabrez/dkr-images/dev-box:0.1 -c zsh
# option 2
# edit the values in `.env` file if you like(defaults should work fine)
docker-compose run dev -c zsh
```

Current folder will be mounted inside the container at `~/code`
You can use `vim` inside the container to edit your source code or connect to
the container using VS Code from the host machine

[Install `devcontainer CLI`](https://code.visualstudio.com/docs/remote/devcontainer-cli)

```sh
devcontainer open .
```

## Pre-requisites to build the image

+ Hashicorp Packer
+ Docker CLI

## Build the image

Edit the values in `vars.auto.pkrvars.hcl.example` & rename it to `vars.auto.pkrvars.hcl`
Run the following command:

```sh
packer build .
```
