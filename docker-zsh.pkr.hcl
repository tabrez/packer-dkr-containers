packer {

  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
    pull   = false
    commit = true
}

build {
  name        = "base-${var.image_name}"
  description = "build container image with git, zsh, vim support"

  source "docker.ubuntu" {
    image  = "${var.from_image}"
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update -y",
      "apt-get install -y python-is-python3"
    ]
  }

  provisioner "ansible" {
    playbook_file = "./playbook.yml"
    extra_arguments = [
      "-vv",
      "--extra-vars",
      "user=${var.linux_username}",
      "--extra-vars",
      "password=${var.linux_password}",
      "--extra-vars",
      "git_name='${var.git_name}'",
      "--extra-vars",
      "git_email=${var.git_email}",
      "--extra-vars",
      "gitconfig_url=${var.gitconfig_url}",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "base-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-tag" {
      repository = "${var.registry_url}/${var.registry_project_name}/base-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-push" {
      login          = true
      login_server   = "${var.registry_url}"
      login_username = "${var.registry_username}"
      login_password = "${var.registry_password}"
    }
  }
}

build {
  name        = "nodejs-${var.image_name}"
  description = "build container image with nodejs support"

  source "docker.ubuntu" {
    image  = "base-${var.image_name}:${var.image_tag}"
    changes = [
      "USER ${var.linux_username}",
      "WORKDIR /home/${var.linux_username}"
    ]
  }

  provisioner "ansible" {
    playbook_file = "./nodejs.yml"
    extra_arguments = [
      "-vv",
      "--extra-vars",
      "user=${var.linux_username}",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "nodejs-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-tag" {
      repository = "${var.registry_url}/${var.registry_project_name}/nodejs-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-push" {
      login          = true
      login_server   = "${var.registry_url}"
      login_username = "${var.registry_username}"
      login_password = "${var.registry_password}"
    }
  }
}


build {
  name        = "python-${var.image_name}"
  description = "build container image with python support"

  source "docker.ubuntu" {
    image  = "base-${var.image_name}:${var.image_tag}"
    changes = [
      "USER ${var.linux_username}",
      "WORKDIR /home/${var.linux_username}"
    ]
  }

  provisioner "ansible" {
    playbook_file = "./python.yml"
    extra_arguments = [
      "-vv",
      "--extra-vars",
      "user=${var.linux_username}",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "python-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-tag" {
      repository = "${var.registry_url}/${var.registry_project_name}/python-${var.image_name}"
      tags       = ["${var.image_tag}"]
    }

    post-processor "docker-push" {
      login          = true
      login_server   = "${var.registry_url}"
      login_username = "${var.registry_username}"
      login_password = "${var.registry_password}"
    }
  }
}
