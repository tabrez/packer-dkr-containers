export DEBIAN_FRONTEND=noninteractive
apt-get update -y
apt-get install -y acl sudo software-properties-common python-is-python3
apt-add-repository -y ppa:ansible/ansible
apt-get install -y ansible
