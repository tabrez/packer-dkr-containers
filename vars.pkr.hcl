# linux
variable "linux_username" {
  type    = string
  default = "ubuntu"
}

variable "linux_password" {
  type    = string
  default = "$6$bKAgpe6igtyx75hY$URrzh/8cN7KtHqALVbymQQuDoWvbIErWal6T0UxaPvdikfAZsXVJXo6ePRoxeuxLguPvOayoPOiYXIRAkKt.g1"
}

# git
variable "git_name" {
  type = string
}

variable "git_email" {
  type = string
}

variable "gitconfig_url" {
  type    = string
  default = "https://gist.githubusercontent.com/tabrez/73446db8b9d562d2bdcf9d47d2adedc9/raw/"
}

# docker
variable "from_image" {
  type    = string
  default = "ubuntu:impish"
}

variable "image_name" {
  type    = string
  default = "dev-box"
}

variable "image_tag" {
  type    = string
  default = "0.1"
}

# container registry
variable "registry_url" {
  type    = string
  default = "registry.gitlab.com"
}

variable "registry_project_name" {
  type    = string
  default = "tabrez/dkr-images"
}

variable "registry_username" {
  type    = string
  default = "dkr-image-user"
}

variable "registry_password" {
  type = string
}
